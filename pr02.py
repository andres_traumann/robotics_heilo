import cv2
import nxt
cv2.namedWindow("Sample")
cap = cv2.VideoCapture(0)
cap.set(3, 640)
cap.set(4, 480)
ret, img=cap.read()
for j in range(0,640):
    for i in range(0,480):
        if((img[i][j][0].astype(float)+img[i][j][1].astype(float)+img[i][j][2].astype(float))<180):
            img[i][j]=[0,0,0]
        else:
            img[i][j]=[255,255,255]
cv2.imshow("Sample", img)
while True:    
    ch = cv2.waitKey(5)
    # Kill program when ESC pressed
    if ch == 27:
        break
cv2.destroyAllWindows()
